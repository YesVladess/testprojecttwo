package com.company;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

/**
 * #2 Создайте ​т​екстовый ​​файл, ​​в ​к​отором ​​есть ​з​начения ​​от ​​0 ​​до ​​20 ​​в ​​произвольном порядке​(​перемешаны).
 * ​​Значения ​​указаны ​​через ​з​апятую ​​без ​​пробелов.
 * ● Прочитайте ​​файл, ​​отфильтруйте ​з​начения ​​по ​​возрастанию​ ​и ​​выведите результат ​​в ​к​онсоль.
 * ● Прочитайте ​​файл, ​​отфильтруйте ​з​начения ​​по убыванию ​и ​​выведите результат ​​в ​к​онсоль.
 */


public class Main {

    public static void main(String[] args) {
        sortFromFile();
    }

    public static void sortFromFile() {


        // Считываем в строку
        String line = "";
        try {
            Scanner in = new Scanner(new File("/Users/vlad/IdeaProjects/TestProjectTwo/src/com/company/numbers.txt"));
            line += in.nextLine();
            in.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        System.out.println("Оригинальный массив: \n" + line );
        // Раскладываем в массив чисел
        int[] numbers = Arrays.stream(line.split(",")).mapToInt(Integer::parseInt).toArray();
        // Сортируем по возрастанию
        sort(numbers);
        // Выводим
        System.out.println("По возрастанию:");
        for (int i=0;i<numbers.length;i++)
            System.out.print(numbers[i] + ",");
        // Выводим в обратном порядке
        System.out.println("\nПо убыванию:");
        for (int i=numbers.length-1;i>=0;i--)
            System.out.print(numbers[i] + ",");
    }


    public static void sort(int[] arr) {
        for (int min = 0; min < arr.length - 1; min++) {
            int least = min;
            for (int j = min + 1; j < arr.length; j++) {
                if (arr[j] < arr[least])
                    least = j;
            }
            int tmp = arr[min];
            arr[min] = arr[least];
            arr[least] = tmp;
        }
    }


}
